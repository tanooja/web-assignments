//clone
const clone = obj => {
  const result = {};
  for (let k in obj) result[k] = obj[k];
  return result;
};
console.log(clone({ x: 1, y: 2 }));


//assoc
const assoc = (k, v, obj) => {
  const result = clone(obj);
  result[k] = v;
  return result;
};
console.log(assoc("c", 3, { a: 1, b: 2 }));


//dissoc
const dissoc = (k, obj) => {
  const result = {};
  for (const i in obj) if (i !== k) result[i] = obj[i];
  return result;
};
console.log(dissoc("b", { a: 1, b: 2, c: 3 }));

//invert
const invert = obj => {
  let result = {};
  for (let i in obj) {
    result[obj[i]] = i;
  }
  return result;
};
console.log(invert({ first: "alice", second: "jake" }));
