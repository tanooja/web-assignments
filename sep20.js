//squaring
const square = arr => {
  let result = [];
  for (let i = 0; i < arr.length; i++) {
    result.push(arr[i] * arr[i]);
  }
  return result;
};
console.log(square([1, 2, 3, 4]));


//even numbers of an array

const even = arr => {
  let result = [];
  for (let i = 0; i < arr.length; i++) {
    result.push(arr[i] % 2 == 0);
  }
  return result;
};
console.log(even([1, 2, 3, 4]));

//sum of an array

const sum = arr => {
  let sum = 0;
  for (let i = 0; i < arr.length; i++) {
    sum = sum + arr[i];
  }
  return sum;
};
console.log(sum([1, 2, 3, 4]));

//max of an array
const max = arr => {
  let max = 0;
  for (let i = 0; i < arr.length; i++) {
    arr[i] > max;
    max = arr[i];
  }
  return max;
};
console.log(max([1, 2, 3, 4]));


//index of value in an array

const index = (arr, val) => {
  for (let i = 0; i <= arr.length; i++) {
    if (arr[i] == val) {
      return arr[i];
    }
  }
};



//repeat
const repeat = (n, x) => {
  const result = [];
  for (let i = 0; i < n; i++) {
    result.push(x);
  }
  return result;
};
console.log(repeat(5, "*"));


//power
const power = (x, y) => {
  var result = 1;
  for (let i = 1; i <= y; i++) result = result * x;
  if (y == 0) return 1;
  return result;
};
console.log(power(3, 0));


//splitting at
const splitAt = (arr, index) => {
  const take = (n, arr) => {
    const result = [];
    for (let i = 0; i < n; i++) result.push(arr[i]);
    return result;
  };
  console.log(take(4, [1, 2, 3, 4, 5, 6, 7, 8]));
  const first = take(arr, index);
  const drop = (n, arr) => {
    const result = [];
    for (let i = n; i < arr.length; i++) result.push(arr[i]);
    return result;
  };
  console.log(drop(4, [1, 2, 3, 4, 5, 6, 7, 8]));
  const second = drop(arr, index);
  return [first, second];
};
console.log(splitAt([1, 2, 3, 4, 5], 3));




